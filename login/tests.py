
from django.contrib.auth.models import User
from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .views import *


class loginTest(TestCase):
    def test_login_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_login_view(self):
        found = resolve('/')
        self.assertEqual(found.func, login)

    def test_using_login_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login.html')
        
    def test_logout_url_exists(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_hello_url_exists(self):
        response = Client().get('/hello/')
        self.assertEqual(response.status_code, 302)

    def test_wrong_username_message(self):
        response = Client().get('/?failed')
        self.assertIn('Wrong username/password', response.content.decode())

    def test_need_login_message(self):
        response = Client().get('/?reqlogin')
        self.assertIn('You need to log in first', response.content.decode())

    def test_logged_out_message(self):
        response = Client().get('/?loggedout')
        self.assertIn('Logout successful', response.content.decode())

    def test_login_logout(self):
        client = Client()
        username = 'yoga'
        password = 'yoga123'
        User.objects.create_user(username=username, password=password)
        # Login
        response = client.post('/login/', {
            'username': username,
            'password': password
        })
        # Test if login successful
        response = client.get('/hello/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Selamat datang, yoga.', response.content.decode())
        # Logout
        response = client.get('/logout/')
        # Test if logout successful
        response = client.get('/hello/')
        self.assertEqual(response.status_code, 302)
